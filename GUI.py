class PyObjectPlus():
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
class WGAimFlash(Flash):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	def updateMarkerPos(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class PyObjectPlusWithWeakReference(PyObjectPlus):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
class WGCrosshairFlash(Flash):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def clearDataProvider(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	def setDataProvider(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class PyAttachment(PyObjectPlusWithWeakReference):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	attached = property()
	castsShadow = property()
	inWorld = property()
	matrix = property()
class WGDirectionIndicatorFlash(Flash):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	position3D = property()
	relativeRadius = property()
	script = property()
	shaders = property()
	showAlways = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class Attachment(PyAttachment):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	attached = property()
	castsShadow = property()
	component = property()
	faceCamera = property()
	inWorld = property()
	matrix = property()
	reflectionVisible = property()
class WGGunMarkerDataProvider(WGAbstractGunMarkerDataProvider):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def setStartSize(*args): pass
	def updateSize(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	positionMatrixProvider = property()
	sizeConstraint = property()
class Simple(PyObjectPlusWithWeakReference):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class WGHitIndicatorFlash(Flash):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def invoke(*args): pass
	def localToScreen(*args): pass
	def offsetRotationElementsInDegree(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	def setGlobalYaw(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class BoundingBox(Simple):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	absoluteSubspace = property()
	alwaysDisplayChildren = property()
	angle = property()
	children = property()
	clipSpaceSource = property()
	clipToBox = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	offsetSubspace = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	source = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class WGMinimapFlashAS3(Flash):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addEntry(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delEntry(*args): pass
	def delShader(*args): pass
	def entryInvoke(*args): pass
	def entrySetActive(*args): pass
	def entrySetMatrix(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def moveEntry(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	def setArenaBB(*args): pass
	def setBackground(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapSize = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class GUIShader(PyObjectPlus):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
class WGSPGCrosshairFlash(Flash):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def clearDataProvider(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	def setDataProvider(*args): pass
	def setPointsBaseScale(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class ClipShader(GUIShader):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def reset(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	currentValue = property()
	delay = property()
	dynValue = property()
	mode = property()
	slant = property()
	speed = property()
	value = property()
class WGSPGGunMarkerDataProvider(WGAbstractGunMarkerDataProvider):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def getPointsInside(*args): pass
	def reset(*args): pass
	def setRelaxTime(*args): pass
	def setStartSize(*args): pass
	def setupConicDispersion(*args): pass
	def setupFlatRadialDispersion(*args): pass
	def update(*args): pass
	def updateSize(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	enableSmoothFiltering = property()
	maxTime = property()
	positionMatrixProvider = property()
	relaxTime = property()
	serverTickLength = property()
	sizeConstraint = property()
	sizeScaleRate = property()
class ColourShader(GUIShader):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def reset(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	colourProvider = property()
	end = property()
	middle = property()
	speed = property()
	start = property()
	value = property()
class WGTankIndicatorFlash(Flash):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_hullMatProv = property()
	wg_inputKeyMode = property()
	wg_turretMatProv = property()
	wg_turretYawConstraints = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class Flash(Simple):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class WGVehicleMarkersCanvasFlashAS3(Flash):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addMarker(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delMarker(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def markerInvoke(*args): pass
	def markerSetActive(*args): pass
	def markerSetMatrix(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	alphaProperties = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	scaleProperties = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class FlashText(Simple):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def reset(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	def stringDimensions(*args): pass
	def stringWidth(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	explicitSize = property()
	filterType = property()
	flip = property()
	focus = property()
	font = property()
	fontSize = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	multiline = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	text = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	underline = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	width = property()
	widthMode = property()
	widthRelative = property()
	wordWrap = property()
class WGVehicleFalloutMarkersCanvasFlashAS3(WGVehicleMarkersCanvasFlashAS3):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addMarker(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delMarker(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def markerInvoke(*args): pass
	def markerSetActive(*args): pass
	def markerSetMatrix(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	alphaProperties = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	movie = property()
	parent = property()
	pixelSnap = property()
	position = property()
	scaleProperties = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	wg_inputKeyMode = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class Frame2(Simple):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class MatrixProvider(PyObjectPlus):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
class Latency(Simple):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class WGVehicleMarkerMP(MatrixProvider):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def startScaleAnimation(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	clamp = property()
	clampMargin = property()
	clipOffset = property()
	constScale = property()
	observer = property()
	scaleRatio = property()
	target = property()
class MatrixShader(GUIShader):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def reset(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	blend = property()
	eta = property()
	target = property()
	wg_targetUpdating = property()
class Window(Simple):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	filterType = property()
	flip = property()
	focus = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	maxScroll = property()
	minScroll = property()
	mouseButtonFocus = property()
	moveFocus = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	scroll = property()
	shaders = property()
	size = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class Text(Simple):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def addChild(*args): pass
	def addShader(*args): pass
	def delChild(*args): pass
	def delShader(*args): pass
	def handleAxisEvent(*args): pass
	def handleKeyEvent(*args): pass
	def handleMouseEvent(*args): pass
	def hasChild(*args): pass
	def localToScreen(*args): pass
	def reSort(*args): pass
	def reset(*args): pass
	def save(*args): pass
	def screenToLocal(*args): pass
	def stringDimensions(*args): pass
	def stringWidth(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	angle = property()
	children = property()
	colour = property()
	colourFormatting = property()
	crossFocus = property()
	dragFocus = property()
	drawWithRestrictedViewPort = property()
	dropFocus = property()
	explicitSize = property()
	filterType = property()
	flip = property()
	focus = property()
	font = property()
	height = property()
	heightMode = property()
	heightRelative = property()
	horizontalAnchor = property()
	horizontalPositionMode = property()
	mapping = property()
	materialFX = property()
	mouseButtonFocus = property()
	moveFocus = property()
	multiline = property()
	parent = property()
	pixelSnap = property()
	position = property()
	script = property()
	shaders = property()
	size = property()
	text = property()
	texture = property()
	textureName = property()
	tileHeight = property()
	tileWidth = property()
	tiled = property()
	verticalAnchor = property()
	verticalPositionMode = property()
	visible = property()
	visibleInAppFocusOnly = property()
	width = property()
	widthMode = property()
	widthRelative = property()
class WorldToClipMP(MatrixProvider):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	onlyFront = property()
	target = property()
class WGAbstractGunMarkerDataProvider(PyObjectPlus):
	def __format__(*args): pass
	def __new__(*args): pass
	def __reduce__(*args): pass
	def __reduce_ex__(*args): pass
	def __sizeof__(*args): pass
	def __subclasshook__(*args): pass
	def setStartSize(*args): pass
	def updateSize(*args): pass
	__doc__ = None
	__members__ = property()
	__methods__ = property()
	positionMatrixProvider = property()
	sizeConstraint = property()
def addRoot(*args): pass
def delRoot(*args): pass
def handleAxisEvent(*args): pass
def handleKeyEvent(*args): pass
def handleMouseEvent(*args): pass
def load(*args): pass
def mcursor(*args): pass
def printRoots(*args): pass
def reSort(*args): pass
def roots(*args): pass
def screenResolution(*args): pass
def setDragDistance(*args): pass
def setResolutionOverride(*args): pass
def syncMousePosition(*args): pass
def wg_setGlyphCacheParams(*args): pass
def wg_setScaleformLogCallback(*args): pass
__doc__ = None
__name__ = 'GUI'
__package__ = None